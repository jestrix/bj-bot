import { config } from 'dotenv';

import Discord, { Client, Events, GatewayIntentBits, Partials, Collection } from 'discord.js';
import cron from 'node-cron';
import {initBrain, brain, brainKeyExistsPromise} from './utils/brain.mjs';
import {importModulesInDir} from "./utils/importModulesInDir.mjs";
import {scheduleAllGagCron} from './utils/gag.mjs';
import {getRandomWeather, scheduleWeatherChange} from './utils/backfire.mjs';
await initBrain();

config(); //for dotenv

// Create a new client instance
const client = new Client({
  partials: [Partials.Message, Partials.Channel, Partials.Reaction],
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.GuildVoiceStates,
  ],
});

// Register all commands in the folder
client.commands = new Collection();
for await (const module of importModulesInDir('commands', { recursive: true })) {
  // Set a new item in the Collection
  // With the key as the command name and the value as the exported module
  client.commands.set(module.data.name, module);
}

// Register events
for await (const event of importModulesInDir('events')) {
  if (event.default.once) {
    client.once(event.default.name, (...args) => event.default.execute(...args));
  } else {
    client.on(event.default.name, (...args) => event.default.execute(...args));
  }
}

// Register scripts in the folder
client.scripts = new Collection();
for await (const scriptModule of importModulesInDir('scripts')) {
  const scriptNames = Object.keys(scriptModule);

  // Loop through each script in the file and add it to the collection
  for (const scriptName of scriptNames) {
    const script = scriptModule[scriptName];

    // Only add objects with regex and execute functions to the scripts collection
    if (script && script.regex && typeof script.execute === 'function') {
      client.scripts.set(scriptName, script);
    }
  }
}

// Register all cron jobs in the folder
for await (const cronJob of importModulesInDir('cron')) {
  // Schedule the cron job
  cron.schedule(cronJob.default.schedule, () => cronJob.default.task(client));
}

// Initialize the weather when the bot starts up
await brainKeyExistsPromise('weather');
brain.data.weather = getRandomWeather();
brain.data.weather.report = null;
await brain.write();
await scheduleWeatherChange();

// Login to Discord with your client's token
client.login(process.env.DISCORD_TOKEN);

// Register the ungag cron jobs
scheduleAllGagCron(client);
