import { Low } from 'lowdb';
import { JSONFile } from 'lowdb/node'
import { config } from 'dotenv';

config(); // Load environment variables from .env file


// Initialize the database
async function initBadJanetBrain() {
  const adapter = new JSONFile('../bad-janet/brain/brain.json');
  const badJanetBrain = new Low(adapter);
  await badJanetBrain.read(); // Load the database from file
  return badJanetBrain;
}

// Export the 'initBrain' function and the 'brain' object for use in other files
export { initBadJanetBrain };
