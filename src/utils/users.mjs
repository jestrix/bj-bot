import Discord from 'discord.js';

/**
 * Get the first mentioned user ID when using robot.respond
 */
export const getFirstUserIdFromMentionOnRespond = function (message) {
  if (message.guildId === null) {
    // In DMs there's only one mention! OR 0 mentions
    // Use the 'on hear' function when this is a DM (no guild)
    return getFirstUserIdFromMentionOnHear(message);
  }
  let mentions = message.mentions;
  if (mentions) {
    // Only works in public messages, which is most commands
    let mentioned_user = mentions.users.first(2)[1]; // User object
    if (mentioned_user) {
      return mentioned_user.id;
    }
  } else {
    // no mentions
    return false;
  }
}


/**
 * Get the first mentioned user ID when using robot.hear
 */
export const getFirstUserIdFromMentionOnHear = function (message) {
  let mentions = message.mentions;
  let mentioned_user = mentions.users.first(); // User object
  if (!mentioned_user) {
    // Not "self" so now we need to extract the ID
    let content = message.content;
    let mention = content.match(/<@\d+>/i);
    if (mention) {
      return mention[0].match(/\d+/)[0];
    }
  } else {
    return mentioned_user.id;
  }
}

export const addRoleByName = async function (user_id, guild, roleName) {
  if (!user_id) {
    throw new Error('No user id provided');
  }
  if (!(guild instanceof Discord.Guild)) {
    throw new Error('Guild passed in needs to be a guild object, not an ID');
  }
  let role = await getRoleByName(guild, roleName);
  let member = await getGuildMemberByUserID(guild, user_id);
  await member.roles.add(role);
}

export const removeRoleByName = async function (user_id, guild, roleName) {
  if (!user_id) {
    throw new Error('No user id provided');
  }
  if (!(guild instanceof Discord.Guild)) {
    throw new Error('Guild passed in needs to be a guild object, not an ID');
  }
  let role = await getRoleByName(guild, roleName);
  let member = await getGuildMemberByUserID(guild, user_id);

  // Check if the member is still in the guild
  if (!member) {
    console.log(`Cannot remove role. User with ID ${user_id} is no longer in the guild`);
    return;
  }

  await member.roles.remove(role);
}

export const getGuildMemberByUserID = async function (guild, user_id) {
  if (!(guild instanceof Discord.Guild)) {
    throw new Error('Guild passed in needs to be a guild object, not an ID');
  }
  return await guild.members.fetch(user_id);
}

export const getRoleByName = async function (guild, roleName) {
  if (!(guild instanceof Discord.Guild)) {
    throw new Error('Guild passed in needs to be a guild object, not an ID');
  }
  const role = await guild.roles.cache.find(role => role.name === roleName);
  if (!role) {
    console.error(`Role not found: ${roleName}`);
    return null;
  }
  return role;
}
