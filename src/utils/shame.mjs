import { brain, brainKeyExists } from './brain.mjs';
import { addRoleByName, getGuildMemberByUserID } from './users.mjs';

import { config } from 'dotenv';
config(); // Load environment variables from .env file

const shame_role = process.env.SHAME_ROLE;

// RETURNS A PROMISE
// Because it has to look up the guild member
export const userHasShameByID = async function(guild, user_id) {
  let guildMember = await getGuildMemberByUserID(guild, user_id);
  // return if the user has the shame role or not
  return guildMember.roles.cache.some(role => role.name === shame_role);
};


export const shameUserRecord = function(user, number, timer, source) {
  let time_now = new Date().getTime();
  let over = '';
  if (timer) {
    over = time_now + timer;
  } else {
    // 24 hours in milliseconds
    over = time_now + 86400000;
  }
  let user_id;
  if (user.id) {
    user_id = user.id;
  } else {
    user_id = user;
  }

  brainKeyExists('count_fail_shame_list');
  brain.data.count_fail_shame_list[user_id] = {
    'user_id': user_id,
    'number': number,
    'received': time_now,
    'timer_ms': timer,
    'over': over,
    'source': source
  };
  brain.write();
}

export const shameUpdate = function(user_id, timer) {
  let time_now = new Date().getTime();
  let over = time_now + timer;
  brainKeyExists('count_fail_shame_list', user_id);
  brain.data.count_fail_shame_list[user_id].over = over;
  brain.data.count_fail_shame_list[user_id].timer_ms = timer;
  brain.write();
}

// Why ask for user ID and not user? Because this is more universal
// Roles require a GUILD MEMBER not USER,
// so if I ask for user_id I can always ensure I'm using the right object
export const addShameByUserId = async function(guild, user_id) {
  await addRoleByName(user_id, guild, shame_role);
}
