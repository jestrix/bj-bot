import { hasConsentByID } from './consent.mjs';
import {brainKeyExistsPromise} from './brain.mjs';


export const getRandomMsg = async function (fail_user) {
  const list = await brainKeyExistsPromise('count_fail_msgs');
  if (!list || list.length === 0) {
    return false;
  }

  let messages = list[fail_user.id];
  let array = [];
  let i = 0;

  // Make a new array of text messages
  // This way I can check consent for each one
  // in case there's a safeword active or
  // consent was removed from someone
  for (let id in messages) {
    // Check for consent by user IDs
    if (await hasConsentByID(messages[id].set_by, fail_user.id)) {
      array[i++] = messages[id];
    }
  }

  if (Object.keys(array).length == 0) {
    return false;
  } else {
    // Return a random message from the list
    return array[Math.floor(Math.random() * array.length)];
  }
}
