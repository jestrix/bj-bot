import { initBrain, brain, brainKeyExists } from './brain.mjs';
import {ChannelType, PermissionFlagsBits } from 'discord.js';
import randomWords from 'random-words'
import { timeUtils } from './time.mjs';

/**
 * Utilities to help make line code happen in more than one place
 * @type {Object}
 */
export const lineUtils = {

  clearLines: async function(user_id, guild) {
    await initBrain();

    if (brain.data.lines[user_id]) {
      delete brain.data.lines[user_id];
      brain.write();
    }
  },

  assignLinesToUser: function(user_id, username, sender_id, guild, lines_todo, line, cmd_channel, goal_time, hide_number_completed) {
    brainKeyExists('lines', user_id);
    if (brain.data.lines[user_id].lines_todo) {
      return '\n**That user already has lines assigned to them. These lines have not been assigned.**'
    } else if (lines_todo) {
      return lineUtils.newLines(user_id, username, sender_id, guild, lines_todo, line, cmd_channel, goal_time, hide_number_completed);
    }
  },

  newLines: async function(user_id, username, sender_id, guild, lines_todo, line, cmd_channel, goal_time, hide_number_completed) {
    brainKeyExists('lines', user_id);
    let reply_message = '';

    let toWrite = line;
    let isRandom = false;

    let newLineChannel = await lineUtils.createLineChannel(user_id, username, sender_id, guild);
    setTimeout(function() {
      if (guild.channels.cache.get(newLineChannel.id) != undefined) {
        lineUtils.clearLines(user_id, guild);
        newLineChannel.delete();
        cmd_channel.send(`@${user_id}> failed to complete their lines before the channel expired.`);
      }
    }, 7200000);

    if (!line) {
      toWrite = lineUtils.generateRandomLine(10, 13, 0.5);
      isRandom = true;
      let displayLine = lineUtils.anticheatFilter(toWrite);
      newLineChannel.send(`<@${user_id}> you must write ${lines_todo} random lines. Your first line is:\n\n **\u{200B}${displayLine}\u{200B}**\n\nYou can cancel the task by typing "cancel". This channel will timeout in 2 hours. ${goal_time > 0 ? '\n\n**You have ' + timeUtils.secsToTime(goal_time) + ' (hh:mm:ss) to complete this task!**' : ''}`);
    } else {
      let displayLine = lineUtils.anticheatFilter(toWrite);
      newLineChannel.send(`<@${user_id}> you must write the following line ${lines_todo} times:\n\n **${displayLine}**\n\nYou can cancel the task by typing "cancel". This channel will timeout in 2 hours. ${goal_time > 0 ? '\n\n**You have ' + timeUtils.secsToTime(goal_time) + ' (hh:mm:ss) to complete this task!**' : ''}`);
    }

    if (lines_todo) {
      brain.data.lines[user_id] = {
        'lines_todo': lines_todo,
        'lines_completed': 0,
        'mistakes': 0,
        'username': username,
        'mistake_on_line': 0,
        'random': isRandom,
        'line': toWrite,
        'channel': newLineChannel.id,
        'cmd_channel': cmd_channel,
        'set_by_id': sender_id,
        'start_time': Date.now(),
        'last_time': Date.now(),
        'goal_time': goal_time,
        'current_cpm': 0,
        'has_cheated': false,
        'hide_number_completed': hide_number_completed,
        'started': false
      };
      brain.write();
      reply_message = lineUtils.generateLineMessage(user_id);
    }

    return reply_message;
  },

  anticheatFilter: function(line) {
    const charMap = {
      ' ': '\u{200B} ',
      'a': 'а', // Cyrillic 'а'
      'e': 'е', // Cyrillic 'е'
    };
    return line.split('').map(c => charMap[c] || c).join('');
  },

  checkForCheating: function(givenLine, cpm) {
    const cheatRegex = /а|е|\u{200B}/gu;
    const containsCheatUnicode = cheatRegex.test(givenLine);
    if(containsCheatUnicode) return true;
    if(cpm > 1000) return true;
    
    return false;
  },

  calculateCPM: function(givenLine, lastTime) {
    const messageLength = givenLine.length;
    const currentTime = Date.now();
    const timeSinceLastLine = currentTime - lastTime;
    const minutesPassed = timeSinceLastLine / 60000;
    const calculatedCPM = messageLength / minutesPassed;
    //console.log("CPM: " + calculatedCPM);
    return calculatedCPM;
  },

  calculateFinalWPM: function(totalCPM, numLines) {
    return Math.round(((((totalCPM / numLines)/5) + Number.EPSILON) * 100))/100;
  },

  generateLineMessage: function(user_id) {
    let linesData = brain.data.lines[user_id];
    let reply_message = '\n';
    reply_message += `Lines to complete: ${linesData.lines_todo}\n`;
    if (linesData.random) {
      reply_message += 'Line to write: Random!\n';
    } else {
      reply_message += `Line to write: \u{200B}${linesData.line}\u{200B}\n`;
    }
    if (linesData.goal_time > 0) {
      reply_message += `Goal time: ${timeUtils.secsToTime(linesData.goal_time)}`;
    }

    return reply_message + '\n\n' + lineMessages.howTo;
  },

  generateRandomLine: function(minWords, maxWords, commaChance) {
    const words = randomWords({min: minWords, max: maxWords});
    let randomSentence = words.join(' ');

    // Add commas randomly
    const randNum = Math.random();
    if (randNum < commaChance) {
      const randomIndex = Math.floor(Math.random() * (words.length - 1));
      randomSentence = randomSentence.replace(words[randomIndex], words[randomIndex] + ',');
    }

    randomSentence = randomSentence.charAt(0).toUpperCase() + randomSentence.slice(1);

    const shouldBeQuestion = Math.random();
    if (shouldBeQuestion < 0.3) {
      randomSentence += '?';
    } else {
      randomSentence += '.';
    }

    return randomSentence;
  },

  insertAtIndex: function(str, index, value) {
    return str.substr(0, index) + value + str.substr(index);
  },

  createLineChannel: async function(user_id, username, sender_id, guild) {
    const everyoneRole = guild.roles.everyone.id;
    const channel = await guild.channels.create({
      name: username + '-lines',
      type: ChannelType.GuildText,
      permissionOverwrites: [
        {type: 'role', id: everyoneRole, deny: [PermissionFlagsBits.ViewChannel]},
        {type: 'member', id: user_id, allow: [PermissionFlagsBits.ViewChannel]},
        {type: 'member', id: sender_id, allow: [PermissionFlagsBits.ViewChannel]},
        {type: 'member', id: process.env.DISCORD_CLIENT_ID, allow: [PermissionFlagsBits.ViewChannel]},
      ],
      parent: process.env.LINES_CATEGORY_ID,
    });
    return channel;
  },
}

export const lineMessages = {
  howTo: 'A new channel has been created just for you to type your lines in.',

  /**
     * Given a user ID, builds a status message for a user's lines
     * @param  string user_id
     */
  statusMessage: function(user_id) {
    brainKeyExists('lines');
    const record = brain.data.lines[user_id];
    if (!record) {
      return `<@${user_id}> currently has no lines to write.`;
    }

    let lines_todo = `<@${user_id}> has ${record.lines_todo} lines to write, `;

    let completed = record.lines_completed ?
            `${record.lines_completed} lines have been completed.\n` :
            `Nothing has been completed yet so there's still ${record.lines_todo} lines to do.`;

    const left = record.lines_todo - record.lines_completed;
    let left_msg = left == record.lines_todo ? '' : `${left} lines left.`;

    return lines_todo + completed + left_msg;
  },
}
