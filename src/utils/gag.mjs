import { config } from 'dotenv';
import {brain, brainKeyExists} from './brain.mjs';
import {addRoleByName, removeRoleByName} from './users.mjs';
config(); // Load environment variables from .env file
import cron from 'node-cron';

const timeout_role = process.env.TIMEOUT_ROLE;

export async function getGagDetails(userId) {
  // Check if a gag entry exists for the user
  let gag = brainKeyExists('gag', userId);

  if (gag) {
    // If it exists, return the gag details
    return gag;
  } else {
    // If it doesn't exist, return null
    return null;
  }
}
export async function gagUserFromInteraction(interaction) {
  const sender = interaction.user;
  const gagged_user = interaction.options.getUser('user');
  const reason = interaction.options.getString('reason');
  let duration = interaction.options.getInteger('duration');
  const guild = interaction.guild;

  await gagUser(sender, gagged_user, reason, duration, guild);
}

export async function gagUser(sender, gagged_user, reason, duration, guild) {
  let gag = brainKeyExists('gag', gagged_user.id);
  gag.duration = duration;
  gag.gagged_by = sender.id;
  gag.reason = reason;

  // Convert duration to milliseconds
  duration = duration * 60 * 1000;

  // Time out the user
  const member = guild.members.cache.get(gagged_user.id);
  await member.timeout(duration, { reason });

  // Get the timeout role
  await addRoleByName(gagged_user.id, guild, timeout_role);

  gag.endTimestamp = member.communicationDisabledUntilTimestamp;
  await brain.write();

  // Schedule to ungag the member when the timeout ends
  const endDate = new Date(gag.endTimestamp);
  await gagMemberCron(endDate, member);
}

export async function scheduleAllGagCron(client) {
  const gags = brainKeyExists('gag');
  const guild = await client.guilds.fetch(process.env.GUILD_ID);

  // Loop through the 'gag' object
  for (const userId in gags) {
    const gag = gags[userId];
    const endDate = new Date(gag.endTimestamp);

    // Make sure the end timestamp is in the future
    if (endDate > Date.now()) {
      const member = await guild.members.fetch(userId);

      // Make sure the member still exists in the guild
      if (member) {
        await gagMemberCron(endDate, member);
      }
    }
  }
}

export async function gagMemberCron(endDate, member) {
  // Create a cron job to ungag the user when the timeout ends
  const job = cron.schedule(`* ${endDate.getMinutes()} ${endDate.getHours()} ${endDate.getDate()} ${endDate.getMonth() + 1} *`, async () => {
    try {
      await ungagMember(member);
    } catch (error) {
      console.error('Failed to ungag user: ', error);
    }
    // Stop the cron job after it has run once
    job.stop();
  });
}

export async function ungagMember(member) {
  await removeRoleByName(member.id, member.guild, timeout_role);
  delete brain.data.gag[member.id];
  await brain.write();
}
