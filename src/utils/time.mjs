export const timeUtils = {
    secsToTime: function(duration) {
        let seconds = Math.floor(duration % 60);
        let minutes = Math.floor((duration / 60) % 60);
        let hours = Math.floor((duration / (60 * 60)) % 24);
    
        hours = (hours < 10) ? '0' + hours : hours;
        minutes = (minutes < 10) ? '0' + minutes : minutes;
        seconds = (seconds < 10) ? '0' + seconds : seconds;
    
        return hours + ':' + minutes + ':' + seconds;
      },
    
      msToTime: function(duration) {
        let seconds = Math.floor((duration / 1000) % 60);
        let minutes = Math.floor((duration / (1000 * 60)) % 60);
        let hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
        let days = Math.floor((duration / (1000 * 60 * 60 * 24)));
    
        let result = '';
        if (days > 0) {
          result += days + (days === 1 ? " day" : " days");
          if(hours > 0 || minutes > 0 || seconds > 0) {
            result += ", ";
          }
        }
        if (hours > 0) {
          result += hours + (hours === 1 ? ' hour' : ' hours');
          if (minutes > 0 || seconds > 0) {
            result += ', ';
          }
        }
        if (minutes > 0) {
          result += minutes + (minutes === 1 ? ' minute' : ' minutes');
          if (hours > 0 && seconds > 0) {
            result += ', ';
          }
          if (seconds > 0) {
            result += ' and ';
          }
        }
        if (seconds > 0) {
          result += seconds + (seconds === 1 ? ' second' : ' seconds');
        }
    
        return result;
      },
    
      msToSeconds: function(duration) {
        return duration / 1000.0;
      },
}