import { Low } from 'lowdb';
import { JSONFile } from 'lowdb/node'
import { initBadJanetBrain } from './badJanetBrain.mjs';

import { config } from 'dotenv';

config(); // Load environment variables from .env file

const adapter = new JSONFile(process.env.BRAIN_FILE);
const brain = new Low(adapter);

// Initialize the database
async function initBrain() {
  await brain.read(); // Load the database from file

  // deal with the brainless condition
  if (brain.data == null) {
    brain.data = {};
  }


  // fill from bad janet
  const badJanetBrain = await initBadJanetBrain();
  if (!brain.data.count_fail_msgs) {
    brain.data.count_fail_msgs = badJanetBrain.data.count_fail_msgs;
  }

  // Write any changes to the database back to the file
  await brain.write();

  return brain.data;
}

// This could probably be written better.
// First, I don't think the .length actually does anything
// because these are objects, right? so length comes back like . . . null or some shit
// but it doesn't hurt anything I think, so fuck it. I'm leaving it
// at least until it annoys me
// or someone else decides to test and change it
//
// Second, is there a way to loop this to handle infinite keys? Probably.
function brainKeyExists(key, key2 = null, key3 = null) {
  // Make sure the consent key exists in the brain
  if (!brain.data[key] || brain.data[key].length == 0) {
    brain.data[key] = {};
  }
  if (!key2) {
    // If there's no other key, return brain with first key
    return brain.data[key];
  } else {
    // More keys, keep making things
    if (!brain.data[key][key2] || brain.data[key][key2].length == 0) {
      brain.data[key][key2] = {};
    }
    if (!key3) {
      // No more keys? Return this object
      return brain.data[key][key2];
    } else {
      // THIRD KEY!
      if (!brain.data[key][key2][key3] || brain.data[key][key2][key3].length == 0) {
        brain.data[key][key2][key3] = {};
      }
      // Return object with third key
      return brain.data[key][key2][key3];
    }
  }
}

/**
 * Same as brainKeyExists, except returns a promise
 * because sometimes you need that, but sometimes you don't
 * so this gives you a choice
 */
function brainKeyExistsPromise(key, key2 = null, key3 = null) {
  return new Promise((resolve, reject) => {
    try {
      const result = brainKeyExists(key, key2, key3);
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
}



// Export the 'initBrain' function and the 'brain' object for use in other files
export { initBrain, brain, brainKeyExists, brainKeyExistsPromise };
