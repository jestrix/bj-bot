// Description:
//   manage the lottery for the corner of shame
//
// Commands:
//   ?joinlottery - you can join the lottery
//   ?leavelottery - you can leave the lottery
//   ?listlottery - lists everyone in the lottery
//   ?drawlottery - draw one name from the lottery, finished the lottery and drops all the names
//   ?draw2lottery - use any number of people to draw

import { config } from 'dotenv';
import {joinLottery, leaveLottery, listLottery, rollLottery} from '../utils/lottery.mjs';
config(); // Load environment variables from .env file


let winners = 1;

/**
 * ?joinlottery
 */
export const joinlottery = {
  regex: /^\?join[lI]ottery/i,
  tag_bot: false,

  execute: async function(message) {
    await joinLottery(null, message);
  }
}

/**
 * ?leavelottery
 */
export const leavelottery = {
  regex: /^\?leavelottery/i,
  tag_bot: false,

  execute: async function(message) {
    await leaveLottery(null, message);
  }
}

/**
 * ?listlottery
 */
export const listlottery = {
  regex: /^\?listlottery/i,
  tag_bot: false,

  execute: async function(message) {
    await listLottery(null, message);
  }
}

/**
 * ?drawlottery / ?draw2lottery
 */
export const drawlottery = {
  regex: /^\?draw\d*lottery/i,
  tag_bot: false,

  execute: async function(message) {
    let input_message = message.content.match(drawlottery.regex)[0];
    let matched = input_message.match(/\d+/i);
    winners = (matched) ? parseInt(matched[0]) || 1 : 1

    await rollLottery(null, message, winners);
  }
}
