// Description:
// ground a user and see their grounding status

import { brain, brainKeyExists } from '../utils/brain.mjs';
import { groundingUtils, groundingMessages } from '../utils/grounding.mjs';
import { getFirstUserIdFromMentionOnRespond } from '../utils/users.mjs';
const admin_room = process.env.BOT_ADMIN_ROOM;

let minutes_todo = 0;
let minutes_minimum = 0;
let percentage = 0;

/**
 *
 * @bot ground <@user> {minutes}m min{session_minutes} {precent}%
 */
export const ground = {
  regex: /^ground <@\d+>/i,
  tag_bot: true,
  execute: async function(message) {
    message.reply('Due to bugs in this command, ' +
      'and for ease of development, ' +
      'it has been moved entirely to the slash command. \n' +
      'Please use `/ground_user` to ground a user.\n\n' +
      'Sorry 😔');
  },
};



/**
 * check on {@user} grounding
 */
export const checkGroundingCommand = {
  regex: /check\s?(on)?\s?<@\d+>\s*grounding/i,
  tag_bot: true,
  execute: async function(message) {
    let user = getFirstUserIdFromMentionOnRespond(message);
    if (user) {
      let reply = groundingMessages.statusMessage(user);
      message.reply(reply);
    } else {
      message.reply('Sorry, I could not find the specified user.');
    }
  }
};


/**
   * This code allows the admin to view a list of all users with groundings.
   *
   * grounding audit
   */
export const groundAudit = {
  regex: /^grounding audit/i,
  tag_bot: false,
  execute: async function(message) {
    if (admin_room && admin_room != message.channel) {
      message.reply("You can't to that here.");
      return;
    }
    let grounding = brain.data.grounding;
    let users = Object.keys(grounding);
    let reply = '';
    for (let i = 0; i < users.length; i++) {
      let user_id = users[i];
      reply += groundingMessages.statusMessage(user_id);
    }
    if (reply) {
      message.reply(reply);
    } else {
      message.reply('No one is currently grounded.');
    }
  }
};


/**
 * This code allows an admin user to remove a user's grounding or update their grounding settings.
 *
 * It checks that the user issuing the command is in the admin room.
 *
 * The command accepts the same arguments as the `ground` command.
 *
 * The grounding is removed if no arguments are provided.
 *
 * Example usage:
 *   @bot unground @user
 *   @bot unground @user 30m min5 50%
 */

export const unground = {
  regex: /^unground <@\d+>/i,
  tag_bot: true,
  execute: async function(message) {
    // check if sender is in the admin room
    if (admin_room && admin_room != message.channel) {
      message.reply("You can't do that here.");
      return;
    }

    let to_user = getFirstUserIdFromMentionOnRespond(message);

    if (!to_user) {
      // was unable to find who this went to
      message.react('❌');
      message.reply('Who?');
    } else {
      // user found, proceed with ungrounding

      getNumbersFromMessage(message);

      // make sure minimum is 12 or less
      if (!groundingUtils.testMinutes(minutes_minimum)) {
        message.reply(groundingMessages.minMinuteMessage);
        return;
      }

      // update the grounding if arguments were provided
      if (minutes_todo > 0 || minutes_minimum || percentage) {
        brainKeyExists('grounding', to_user);

        brain.data.grounding[to_user].minutes_todo = minutes_todo || 1;
        brain.data.grounding[to_user].minutes_min = minutes_minimum || 0;
        brain.data.grounding[to_user].minimum_percentage = percentage || 0;
        brain.write();

        message.reply(`Updated grounding for <@${to_user}>: `
          + `${minutes_todo || 1}m, `
          + `min${minutes_minimum || 0}, `
          + `${percentage || 0}% required.`);
      } else {
        groundingUtils.clearGrounding(to_user, message.guild);
        message.reply(`Removed grounding for <@${to_user}>.`);
      }
    }
  }
};


function getNumbersFromMessage(message) {
  let minutes_todo_pattern = message.content.match(/\d+\s?m(in)?(ute)?s?/i);
  if (minutes_todo_pattern) {
    minutes_todo = minutes_todo_pattern[0].match(/\d+/)[0];
  } else {
    minutes_todo = 0;
  }
  let minutes_minimum_pattern = message.content.match(/min(imum)?\s?\d+/i);
  if (minutes_minimum_pattern) {
    minutes_minimum = minutes_minimum_pattern[0].match(/\d+/)[0];
  } else {
    minutes_minimum = 0;
  }
  let percentage_pattern = message.content.match(/\d+\s?%/);
  if (percentage_pattern) {
    percentage = percentage_pattern[0].match(/\d+/)[0];
    if (+percentage > 100) {
      percentage = 100;
    }
  } else {
    percentage = 0;
  }
}
