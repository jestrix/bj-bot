// Description:
//   give the corner of shame to a user who asks for it
//
// Commands:
//   hubot shame me
//   hubot shame @user

import { EmbedBuilder } from 'discord.js';
import { addShameByUserId, shameUserRecord } from '../utils/shame.mjs';
import { getRandomMsg } from '../utils/failed_count_msg.mjs';
import { brain } from '../utils/brain.mjs';
import { hasConsent } from '../utils/consent.mjs';
import { config } from 'dotenv';
import {backfires} from '../utils/backfire.mjs';
config(); // Load environment variables from .env file

const shame_role = process.env.SHAME_ROLE;
const shame_room = process.env.SHAME_ROOM;
const log_room = process.env.LOGS_ROOM;

const timer_time = 86400000; // 24 hours in milliseconds


export const shame_me_hard = {
  regex: /shame me hard/i,
  tag_bot: true,
  execute: async function (message) {
    let channel = message.channel;
    let task = await getRandomMsg(message.author);
    if (!task) return channel.send("You don't have any count messages set.");
    let embed = new EmbedBuilder()
      .setColor('#0099ff')
      .setTitle('SHAME ON YOU!')
      .addFields({name: "Here's your task.", value: task.text})
    let content = `Heads up <@${message.author.id}>! You've got a special message from <@${task.set_by}>.`;
    channel.send({content: content, embeds: [embed]});

    shameUserRecord(message.author, 0, timer_time, 'shame_me');
    await addShameByUserId(message.guild, message.author.id);
    message.channel.send(`SHAME ON <@${message.author.id}>!`);
  }
}

export const shame_me = {
  regex: /shame me$/i,
  tag_bot: true,
  execute: async function (message) {
    shameUserRecord(message.author, 0, timer_time, 'shame_me');
    await addShameByUserId(message.guild, message.author.id);
    message.channel.send(`SHAME ON <@${message.author.id}>!`);
  }
}

export const shame_you = {
  regex: /shame (on )?you/i,
  tag_bot: true,
  execute: async function (message) {
    message.reply('Nice try. Go to the corner for that!');
    shameUserRecord(message.author, 0, timer_time, 'shame_me');
    await addShameByUserId(message.guild, message.author.id);
    message.channel.send(`SHAME ON <@${message.author.id}>!`);
  }
}

export const shame_user = {
  regex: /shame <@/i,
  tag_bot: true,
  execute: async function (message) {
    let content = message.content;
    // Get the front half of the message
    let front_half = content.match(/shame <@\d+>/i);
    if (front_half) {
      let mentions = message.mentions;
      let mentioned_user = mentions.users.first(2)[1]; // User object

      if (!mentioned_user) {
        // I haven't been able to come up with a way
        // That you can get this far unless you're mentioning the bot
        message.reply('Nice try. Go to the corner for that!');
        shameUserRecord(message.author, 0, timer_time, 'shame_me');
        await addShameByUserId(message.guild, message.author.id);
        message.channel.send(`SHAME ON <@${message.author.id}>!`);
        await message.react('❌');
        return;
      }

      // If mentioned user is the bot, shame user for bot shaming
      if (mentioned_user.bot) {
        message.reply("I don't allow bot-shaming around these parts.");
        shameUserRecord(message.author, 0, timer_time, 'shame_me');
        await addShameByUserId(message.guild, message.author.id);
        message.channel.send(`SHAME ON <@${message.author.id}>!`);
        await message.react('❌');
        return;
      }

      if (await backfires(message.member, mentioned_user)) {
        let reply = '# Backfire!\n';
        reply += `${message.author} tried to shame ${mentioned_user} but it backfired!`;
        shameUserRecord(message.author, 0, timer_time, 'backfire');
        await addShameByUserId(message.guild, message.author.id);
        message.reply(reply);
        return;
      }

      if (await hasConsent(message.author, mentioned_user)) {
        shameUserRecord(mentioned_user, 0, timer_time, 'shame_command');
        await addShameByUserId(message.guild, mentioned_user.id);
        message.channel.send(`SHAME ON <@${mentioned_user.id}>!`);
      } else {
        message.channel.send('You do not have consent to shame that user.');
      }
    }
  }
}

export const transfer_shame = {
  regex: /\?transfer /i,
  tag_bot: false,
  execute: async function (message) {
    let sender = message.author;
    let mentions = message.mentions;
    let to_user = mentions.users.first(); // User object

    if (!to_user) {
      await message.react('❌');
      message.reply('Who?');
    } else {
      let guild = message.guild;
      // First, does the user have the shame role?
      let role = guild.roles.cache.find(role => role.name === shame_role);
      let guildMember = await guild.members.fetch(sender.id);
      if (!guildMember.roles.cache.some(role => role.name === shame_role)) {
        // User does not have the shame role, fail here
        await message.react('🚫');
        message.reply("You can't transfer a role you don't have.");
      } else {
        // User has shame role, can transfer, maybe

        // You know what? Maybe we should disallow certain types of shame to be transferred
        if (brain.data.count_fail_shame_list[sender.id]) {
          if (brain.data.count_fail_shame_list[sender.id].source === 'cheating_on_lines') {
            message.reply('You know how they say that cheaters never prosper? ' +
              'They never transfer shame either. Cheaters keep their shame to themselves! ' +
              'Wait, or count your way out.');
            return;
          }
        }

        // Second, make sure the target isn't already shamed
        let receiver_member = await guild.members.fetch(to_user.id);
        if (receiver_member.roles.cache.some(role => role.name === shame_role)) {
          // The target is already shamed, fail here
          await message.react('🚫');
          message.reply("You can't transfer the role to someone who already has it.");
        } else {
          // The target is not already shamed
          // Third, does the target consent to the user?
          if (!await hasConsent(sender, to_user)) {
            // Target does not consent, stop here
            await message.react('🚫');
            message.reply('You do not have consent to transfer shame to that user.');
          } else {
            // if the person who's giving it away has a shame record
            // base the new record on the old one and delete it, otherwise make a new one
            if (brain.data.count_fail_shame_list[sender.id]) {
              shameUserRecord(
                receiver_member,
                brain.data.count_fail_shame_list[sender.id].number,
                brain.data.count_fail_shame_list[sender.id].timer_ms,
                'transfer'
              );
              delete brain.data.count_fail_shame_list[sender.id];
            } else {
              // make a new shame record for person
              shameUserRecord(receiver_member, 0, timer_time, 'transfer');
            }

            await brain.write();

            // Remove role from giving user
            await guildMember.roles.remove(role);
            // Add role to the target
            await receiver_member.roles.add(role);

            const channel = message.guild.channels.cache.get(shame_room);

            // Send in shame room
            channel.send(`<@${guildMember.id}> has passed on their `
                + `Corner of Shame to you <@${receiver_member.id}> `
                + 'as they are either your Dominant or you have agreed to '
                + 'complete their Corner of Shame punishment on their behalf. '
                + 'Type `?punish` to get started, enjoy.');
            message.delete();

            const log_channel = message.guild.channels.cache.get(log_room);
            log_channel.send(`<@${guildMember.id}> has passed on their `
                + `Corner of Shame to <@${receiver_member.id}> `
            );
          }
        }
      }
    }
  }
}
