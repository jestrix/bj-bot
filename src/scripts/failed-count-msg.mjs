// Description:
//    Tops can add additional consequences for messing up the count
//
// Commands:
//    hubot add/set {one-time} count msg  @user text
//    hubot remove count msg {id}
//    hubot see count msg
//

import {brain, brainKeyExists, brainKeyExistsPromise} from '../utils/brain.mjs';
import {getFirstUserIdFromMentionOnRespond} from '../utils/users.mjs';
import {MessageStacker} from '../utils/classes/MessageStacker.mjs';
import {hasConsentByID} from '../utils/consent.mjs';

const admin_room = process.env.BOT_ADMIN_ROOM;

/**
 * help count message
 */
export const failedCountHelp = {
  regex: /^(help\s?)?count(ing)?\s?(message|msg)s?\??$/i,
  tag_bot: false,
  execute: function(message) {
    message.reply('**Counting Messages (Punishments)**\n' +
      'Add some text that will display with the "fruiting" message. ' +
      'If more than one is added, something random will display. The original purpose of this ' +
      'is to add additional punishments when a user messes up the count, but it could be used ' +
      'for other messages as well such as encouragement, beratement, etc.\n' +
      '\n' +
      '*Requires consent to add messages.*\n' +
      '\n' +
      ':small_blue_diamond:\\`@BJ add count msg @user text message\\` Add a message. ' +
      'If there\'s more than one added, the bot will choose one randomly\n' +
      '\n' +
      ':small_blue_diamond:\\`@BJ add onetime count msg @user text message\\` ' +
      'Add a one-time message. OT Message will be removed after being given.\n' +
      '\n' +
      ':small_blue_diamond:\\`@BJ override count msg @user text message\\` Set one message. ' +
      'Will override any others you had set, so if you had 3 added, and use \\`override\\` you will ' +
      'only have this one remaining. Does not remove or override messages set by other users.\n' +
      '\n' +
      ':small_blue_diamond:\\`@BJ override onetime count msg @user text message\\` same as ' +
      'above except it\'s a one-time message that will be removed after being given\n' +
      '\n' +
      ':small_blue_diamond:\\`@BJ see count msg @user\\` User optional; see either all you\'ve ' +
      'set for a user, or all you\'ve set for all users. Will provide IDs for removing messages from ' +
      'the list. Users cannot look up what messages have been set for them.\n' +
      '\n' +
      ':small_blue_diamond:\\`@BJ remove count msg @user\\` Remove all messages you\'ve set for a user\n' +
      '\n' +
      ':small_blue_diamond:\\`@BJ remove count msg {id}\\` Remove a specific message you\'ve set for a user\n' +
      '\n' +
      'Note that you can use "count" or "counting" and "message" or "msg" and spaces are optional. ' +
      'For example \\`@BJ add counting message\\` works and so does \\`@BJ addCountMsg\\`.\n' +
      'To add surprise messages without your sub knowing, DM the command to Bad Janet, just leave off ' +
      'the @BJ part (you will need to form the @tag using \\`<@user_id>\\` for this to work; ' +
      'ask for help if you don\'t know how).`);\n');
  }
}


/**
 * count message audit
 */
export const failedCountAudit = {
  regex: /^count(ing)?\s?(message|msg)s?\s?audit$/i,
  tag_bot: true,
  execute: function(message) {
    if (admin_room && admin_room !== message.channel.id) {
      message.reply("You can't to that here.");
    } else {
      let reply = 'Messages:';
      let messages = brainKeyExists('count_fail_msgs');
      let this_message = '';
      let temp_reply = '';
      for (let user_id in messages) {
        for (let key in messages[user_id]) {
          this_message = `id: ${messages[user_id][key].id}\n` +
            `is_one_time: ${messages[user_id][key].is_one_time}\n` +
            `text: ${messages[user_id][key].text}\n` +
            `date_set: ${messages[user_id][key].date_set}\n` +
            `set_by: <@${messages[user_id][key].set_by}>\n` +
            `given_to: <@${messages[user_id][key].given_to}>`;
          temp_reply = reply + this_message
          if (temp_reply.length > 2000) {
            message.reply(reply);
            reply = this_message;
          } else {
            reply = temp_reply
          }
        }
      }
      message.reply(reply);
    }
  }
}


/**
 * delete count message (id)
 */
export const failedCountdelete = {
  regex: /^force (remove|delete)\s?count(ing)?\s?(message|msg)/i,
  tag_bot: false,
  execute: async function(message) {
    if (admin_room && admin_room !== message.channel.id) {
      message.reply("You can't to that here.");
    } else {
      let id_match = message.content.match(/\d+/);
      let id = id_match[0];
      let messages = brainKeyExists('count_fail_msgs');
      for (let user_id in messages) {
        for (let key in messages[user_id]) {
          if (key === id) {
            delete brain.data.count_fail_msgs[user_id][id];
            await brain.write();
            message.reply(id + ' deleted');
            return;
          }
        }
      }
      message.reply('Not found; nothing deleted.');
    }
  }
}


/**
 * add/override count message (id)
 */
export const failedCountAdd = {
  regex: /(add|override)\s?(one[-\s]?time )?count(ing)?\s?(message|msg)\s?<@/i,
  tag_bot: true,
  execute: async function(message) {
    console.log('here');
    // Get @user from mentions
    let mentioned_user_id = getFirstUserIdFromMentionOnRespond(message);

    let sender = message.author; // User object

    // Check consent
    if (await hasConsentByID(sender.id, mentioned_user_id)) {
      // add to brain
      let id = (new Date()).getTime();

      // Get the message text
      let match = message.content.match(/(message|msg) <@\d+> .*/is);
      let text = match[0].replace(/(message|msg) <@\d+> /is, '');
      // Is one time?
      let is_one_time = message.content.match(/one[-\s]?time/i);
      // Create brain object if needed and get brain object
      let messages = brainKeyExists('count_fail_msgs', mentioned_user_id);

      // Save regex except it only matches on "set"
      let overwrite = message.content.match(/override\s?(one[-\s]?time )?count(ing)?\s?(message|msg)/i);
      if (overwrite) {
        // If using "set" instead of "add" overwrite all other messages this top set
        await clearUserMessages(mentioned_user_id, sender.id);
      }

      // Set brain object information
      messages[id] = {
        'id': id,
        'is_one_time': (!!is_one_time),
        'text': text,
        'date_set': new Date(),
        'set_by': sender.id,
        'given_to': mentioned_user_id,
        'given_count': 0,
        'dm_only': false // TODO: allow for DM messages
      }
      // Save!
      await brain.write();
      await message.react('✅');
      // How many messages are set?
      let count = countUserMessages(mentioned_user_id);

      let reply_text = "I've added the counting message.\n" +
        `<@${mentioned_user_id}> currently has ${count} counting message(s) set.`;
      if (count > 1) {
        reply_text += ' One will be chosen randomly if they mess up the count.';
      }

      await message.reply(reply_text);
    } else {
      await message.react('🚫');
      await message.reply('You do not have consent to add counting messages to that user.');
    }
  }
}


/**
 * remove count message (id)
 */
export const failedCountRemv = {
  regex: /remove\s?count(ing)?\s?(message|msg)s? /i,
  tag_bot: true,
  execute: async function (message) {
    // Get @user from mentions
    let mentioned_user_id = getFirstUserIdFromMentionOnRespond(message);
    let sender = message.author; // User object

    if (mentioned_user_id) {
      await brainKeyExistsPromise('count_fail_msgs', mentioned_user_id);
      // A user was given!
      let delete_count = clearUserMessages(mentioned_user_id, sender.id)

      // TODO: if there weren't any, say that.
      if (delete_count > 0) {
        message.reply(`${delete_count} messages cleared for <@${mentioned_user_id}>.`);
      } else {
        message.reply('No messages found to clear.');
      }
    } else {
      // Look for an ID
      let match = message.content.match(/remove\s?count(ing)?\s?(message|msg)s? \d+/is);
      let matched_id = match[0].replace(/remove\s?count(ing)?\s?(message|msg)s? /is, '');
      let found_message = findMessageById(matched_id);
      if (found_message) {
        // TODO: also allow admin to remove messages. Maybe only in the admin bot room
        if (sender.id === found_message.set_by) {
          // Delete found_message; I don't know why this doesn't work
          delete brain.data.count_fail_msgs[found_message.given_to][matched_id]; // Works
          await brain.write();
          await message.react('✅');
        } else {
          message.reply("Wait. You weren't the one who set that message! " +
            `Only <@${found_message.set_by}> can remove ${matched_id}!`);
        }
      } else {
        message.reply('I couldn\'t find that message.');
      }
    }
  }
}


/**
 * see count messages @user
 */
export const failedCountSee = {
  regex: /see\s?count(ing)?\s?(message|msg)s?/i,
  tag_bot: true,
  execute: async function (message) {
    // Get @user from mentions
    let mentioned_user_id = getFirstUserIdFromMentionOnRespond(message);
    let sender = message.author; // User object
    if (mentioned_user_id) {
      // A user was given!
      let messages = brainKeyExists('count_fail_msgs', mentioned_user_id);
      let prepend = `You have set the following messages on <@${mentioned_user_id}>: \n`;
      const stacker = new MessageStacker(message, prepend);
      for (let id in messages) {
        if (messages[id].set_by === sender.id) {
          let m = messages[id]
          stacker.appendOrSend('💠' + id + (m.is_one_time ? '(one time)' : '') + ': ' + m.text + '\n');
        }
      }
      stacker.finalize('*You can remove any of these by sending me `@BJ remove counting message id_number`*',
        'You don\'t have any message set for that user at the moment.'
      );
    } else {
      // Need to sort through to find all your messages
      let messages = brainKeyExists('count_fail_msgs');
      const stacker = new MessageStacker('You have set the following messages: \n');
      for (let sub_id in messages) {
        for (let id in messages[sub_id]) {
          if (sender.id === messages[sub_id][id].set_by) {
            let m = messages[sub_id][id]
            stacker.appendOrSend(`💠<@${sub_id}> (${id})` + (m.is_one_time ? ' (one time)' : '') + ': ' + m.text + '\n');
          }
        }
      }
      stacker.finalize('*You can remove any of these by sending me `@BJ remove counting message id_number`*',
        'You don\'t have any message set at the moment.'
      );
    }
  }
}


function findMessageById(provided_id) {
  let messages = brainKeyExists('count_fail_msgs');
  for (let sub_id in messages) {
    for (let id in messages[sub_id]) {
      if (id === provided_id) {
        return messages[sub_id][id];
      }
    }
  }
}

async function clearUserMessages(sub_id, top_id) {
  let user_messages = brainKeyExists('count_fail_msgs', sub_id);
  let count = 0;
  for (let id in user_messages) {
    if (user_messages[id].set_by === top_id) {
      count++;
      delete user_messages[id];
    }
  }
  await brain.write();
  return count;
}

function countUserMessages(user_id) {
  let user_messages = brainKeyExists('count_fail_msgs', user_id);
  let count = 0;
  for (let id in user_messages) {
    count++;
  }
  return count;
}
