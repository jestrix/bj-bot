

const reply = 'This is a <@929783745513406504> command and isn\'t on this bot yet.';


export const consent_to = {
  regex: /consent (to )?/i,
  tag_bot: true,
  execute: function(message) {
    message.reply(reply);
  }
}

export const end_consent = {
  regex: /end consent (to )?/i,
  tag_bot: true,
  execute: function(message) {
    message.reply(reply);
  }
}

export const safeword = {
  regex: /(safeword|red|pineapple)/i,
  tag_bot: true,
  execute: function(message) {
    message.reply(reply);
  }
}

export const end_safeword = {
  regex: /end safeword/i,
  tag_bot: true,
  execute: function(message) {
    message.reply(reply);
  }
}

export const received_consent = {
  regex: /received consent/i,
  tag_bot: true,
  execute: function(message) {
    message.reply(reply);
  }
}

export const given_consent = {
  regex: /given consent/i,
  tag_bot: true,
  execute: function(message) {
    message.reply(reply);
  }
}
