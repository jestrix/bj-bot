import { execute } from '../../commands/ping.mjs';
import { getMessageMock } from '../../utils/mocks.mjs';

test('sends "Pong!" message when the ping command is executed', async () => {
  const messageMock = getMessageMock();

  await execute(messageMock);

  expect(messageMock.reply).toHaveBeenCalledTimes(1);
  expect(messageMock.reply).toHaveBeenCalledWith('Pong!');
});
