import { SlashCommandBuilder } from 'discord.js';
import {lineUtils} from '../../utils/lines.mjs';
import { hasConsent } from '../../utils/consent.mjs';
import {backfires} from '../../utils/backfire.mjs';

export const data = new SlashCommandBuilder()
  .setName('assign_lines')
  .setDescription('Assigns a line to a user')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to assign lines to.')
    .setRequired(true))
  .addIntegerOption(option => option.setName('num_lines')
    .setDescription('The number of lines the user should type.')
    .setRequired(true))
  .addStringOption(option => option.setName('line')
    .setDescription('The line you want the user to write. If left empty, random lines will be generated.')
    .setRequired(false))
  .addStringOption(option => option.setName('personalized_instructions')
    .setDescription('Special instructions for the lines (free text) - please use ||spoiler text|| for anything NSFW.')
    .setRequired(false))
  .addIntegerOption(option => option.setName('goal_time')
    .setDescription('Set a time the user is required to finish their lines in (in seconds).')
    .setMinValue(1)
    .setRequired(false))
  .addBooleanOption(option => option.setName('hide_number_completed')
    .setDescription('Set whether the number of lines completed should be hidden')
    .setRequired(false)
  )

export async function execute(interaction) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const lines_todo = interaction.options.getInteger('num_lines');
  const line = interaction.options.getString('line');
  const personalized_instructions = interaction.options.getString('personalized_instructions');
  const command_channel = interaction.channel.id;
  const goal_time = interaction.options.getInteger('goal_time');
  const hide_number_completed = interaction.options.getBoolean('hide_number_completed');
  let should_hide_completed = false;

  if (hide_number_completed) should_hide_completed = true;

  if (line !== null && line.toLowerCase() === 'cancel') {
    await interaction.reply({
      content: 'You cannot assign someone that line. Try another one.',
      ephemeral: true
    });
  }

  if (await backfires(interaction.member, to_user)) {
    await interaction.deferReply();
    let reply = '# Backfire!\n';
    reply += `***<@${sender.id}> tried to give <@${to_user.id}> lines to write but it backfired!***\n`;
    reply += await lineUtils.assignLinesToUser(sender.id, sender.username, sender.id, interaction.guild, lines_todo, line, command_channel, goal_time, should_hide_completed);
    if (personalized_instructions) {
      reply += `\n\n**You have special instructions:** ${personalized_instructions}`;
    }
    await interaction.editReply(reply);

    return;
  }

  if (!await hasConsent(sender, to_user)) {
    await interaction.reply({
      content: 'You do not have consent to set lines for that user.',
      ephemeral: true
    });
    return;
  }

  await interaction.deferReply();

  let reply = `***<@${sender.id}> has given <@${to_user.id}> lines to write!***\n`;
  reply += await lineUtils.assignLinesToUser(to_user.id, to_user.username, sender.id, interaction.guild, lines_todo, line, command_channel, goal_time, should_hide_completed);
  if (personalized_instructions) {
    reply += `\n\n**You have special instructions:** ${personalized_instructions}`;
  }
  await interaction.editReply(reply);
}
