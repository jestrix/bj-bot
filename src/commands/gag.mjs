import { SlashCommandBuilder } from 'discord.js';
import {hasConsent} from '../utils/consent.mjs';
import {gagUser, gagUserFromInteraction} from '../utils/gag.mjs';
import {backfires} from '../utils/backfire.mjs';

const TIMEOUT_MAX_MINUTES = 15;

export const data = new SlashCommandBuilder()
  .setName('gag')
  .setDescription('Uses the discord "timeout" feature to prevent a user for talking in the server (max 15 minutes).')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you want to gag')
    .setRequired(true))
  .addIntegerOption(option => option.setName('duration')
    .setDescription('The duration for timeout in minutes')
    .setRequired(true))
  .addStringOption(option => option.setName('reason')
    .setDescription('A fun reason')
    .setRequired(false))
;

export async function execute(interaction) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const reason = interaction.options.getString('reason');
  let duration = interaction.options.getInteger('duration');

  // check if duration is valid
  if (duration > TIMEOUT_MAX_MINUTES) {
    await interaction.reply({
      content: '15 minutes is the maximum time you can gag someone with this command.',
      ephemeral: true,
    });
    return;
  }

  if (await backfires(interaction.member, to_user)) {
    await interaction.deferReply();
    try {
      await gagUser(sender, sender, 'Backfire!', duration, interaction.guild);
      let reply = '# Backfire!\n' +
        `${sender} tried to gag ${to_user} but it backfired and ` +
        `now ${sender} is gagged for ${duration} minutes!`;
      await interaction.editReply(reply);
    } catch (error) {
      console.error(`Failed to backfire gag ${to_user.username}: `, error);
      await interaction.editReply({
        content: `Failed to backfire gag ${to_user.username}. Please try again later.`
      });
    }
    return;
  }

  // check consent
  if (!await hasConsent(sender, to_user)) {
    await interaction.reply({content: 'You do not have consent to gag that user.', ephemeral: true});
    return;
  }

  await interaction.deferReply();

  // time the user out here:
  try {
    await gagUserFromInteraction(interaction);

    let message = `${sender} gagged ${to_user} for ${duration} minutes.`;
    if (reason) {
      message += `\nReason: ${reason}`;
    }

    // Reply to the interaction
    await interaction.editReply({ content: message });
  } catch (error) {
    console.error(`Failed to gag ${to_user.username}: `, error);
    await interaction.editReply({ content: `Failed to gag ${to_user.username}. Please try again later.` });
  }
}
