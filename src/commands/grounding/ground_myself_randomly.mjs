import { SlashCommandBuilder } from 'discord.js';
import { groundingUtils, groundingMessages } from '../../utils/grounding.mjs';
import { addRoleByName } from '../../utils/users.mjs';
import _ from 'lodash';

const grounded_role = process.env.GROUNDED_ROLE;

export const data = new SlashCommandBuilder()
  .setName('ground_myself_randomly')
  .setDescription('Grounds yourself for some amount of time')
  .addIntegerOption(option => option.setName('minutes')
    .setDescription('The maximum amount of time you\'ll tolerate in minutes (default 60).')
    .setRequired(false))
  .addIntegerOption(option => option.setName('minimum_session')
    .setDescription('The minimum session duration in minutes (will randomize if not set)')
    .setRequired(false))
  .addNumberOption(option => option.setName('completion_percentage')
    .setDescription('The required percentage of completion (will randomize if not set)')
    .setRequired(false));

export async function execute(interaction) {
  const sender = interaction.user;
  let minutes_max = interaction.options.getInteger('minutes') || 60;
  let minutes_minimum = interaction.options.getInteger('minimum_session');
  let percentage = interaction.options.getNumber('completion_percentage');

  // make sure minimum is 12 or less
  if (minutes_minimum && !groundingUtils.testMinutes(minutes_minimum)) {
    interaction.reply(groundingMessages.minMinuteMessage);
    return;
  }

  // add grounded role
  await addRoleByName(sender.id, interaction.guild, grounded_role);

  // generate random values if not provided
  if (!minutes_minimum) {
    minutes_minimum = _.random(1, 12, false);
  }
  if (!percentage) {
    percentage = _.random(1, 100, false);
  }

  const minutes_todo = _.random(1, minutes_max, false);

  // issue grounding
  let embed = await groundingUtils.groundUser(sender.id, minutes_todo, minutes_minimum, percentage);

  await interaction.reply({
    content: '🎲 **Random Grounding!** 🎲',
    embeds: [embed]
  });
}
