import { SlashCommandBuilder } from '@discordjs/builders';
import { EmbedBuilder } from 'discord.js';
import {addShameByUserId, shameUserRecord} from '../utils/shame.mjs';
import {lineUtils} from '../utils/lines.mjs';

export const data = new SlashCommandBuilder()
  .setName('hotpotato')
  .setDescription('Starts a hot potato game.')
  .addStringOption(option =>
    option.setName('stakes')
      .setDescription('The stakes for the game')
      .setRequired(true)
      .addChoices(
        { name: 'Shame', value: 'shame' },
        // TODO: make grounding an option
        //{ name: 'Grounding', value: 'grounding' },
        { name: 'Lines', value: 'lines' },
        // TODO: make a surprise option
        //{ name: 'Surprise!', value: 'surprise' },
        { name: 'Manual Entry', value: 'manual' }
      )
  );

export async function execute(interaction) {
  const stakes = interaction.options.getString('stakes');
  let followUpText = '';
  await interaction.deferReply();
  await interaction.editReply("Starting a game...");

  let stakesDetails = ''; // This variable will hold the details of the stakes
  let gameSetupSuccess = true; // Flag variable to track game setup status

  let punishment = {};

  switch(stakes) {
    case 'shame':
      stakesDetails = 'Loser is shamed!';
      punishment = {'shame': true};
      break;

    case 'lines':
      followUpText = 'Please tell me the number of lines (you have 60 seconds).';
      await interaction.followUp(followUpText);

      const filter = m => m.author.id === interaction.user.id;

      try {
        const collected = await interaction.channel.awaitMessages({ filter, max: 1, time: 60000, errors: ['time'] });
        const numberOfLines = parseInt(collected.first().content, 10);

        if (Number.isInteger(numberOfLines) && numberOfLines > 0) {
          stakesDetails = `Loser does ${numberOfLines} random word lines!`;

          punishment = {'lines': numberOfLines};
        } else {
          interaction.followUp('Invalid input. Please enter a positive integer.');
          gameSetupSuccess = false; // Game setup failed
        }
      } catch (collected) {
        interaction.followUp('Game setup cancelled. You did not enter the number of lines.');
        gameSetupSuccess = false; // Game setup failed
      }

      break;

    case 'manual':
      followUpText = 'Please tell me what the stakes will be (you have 60 seconds).';
      await interaction.followUp(followUpText);

      const filter2 = m => m.author.id === interaction.user.id;

      try {
        const collected2 = await interaction.channel.awaitMessages({ filter: filter2, max: 1, time: 60000, errors: ['time'] });
        stakesDetails = collected2.first().content;
        punishment = {'manual': stakesDetails};
      } catch (collected) {
        interaction.followUp('Game setup cancelled. You did not enter the stakes.');
        gameSetupSuccess = false; // Game setup failed
      }

      break;

    default:
      followUpText = 'Invalid stakes option provided.';
      gameSetupSuccess = false; // Game setup failed
      break;
  }

  // If the game setup is successful, display the game start embed
  if (gameSetupSuccess) {
    const gameEmbed = new EmbedBuilder()
      .setColor('#0099ff')
      .setTitle('Hot Potato is starting!')
      .setDescription(`Stakes of the game are:\n> ${stakesDetails}\n\n` +
        'Game starts in 60 seconds! React 🥔 to this message to join in!'
      )
      .addFields(
        {name: 'Instructions', value: 'When the game starts, only the people ' +
          'who have joined can play. ' +
          'All other users will be ignored by the bot. Make sure to join. Type fast!'
        }, {
        name: 'Commands', value: '* `pass` pass to a random person\n' +
          '* `pass @user` pass to a specific person\n' +
          '* `take` take the potato away from whoever has it'
        }
      )
      .setFooter({text:
        'The potato will explode somewhere between 15 and 50 seconds from start of game. Good luck!'
      })
    ;

    // The bot sends the embed message
    const gameMessage = await interaction.followUp({ embeds: [gameEmbed] });

    // The bot reacts to the message with a potato emoji
    await gameMessage.react('🥔');

    const filter = (reaction, user) => {
      return reaction.emoji.name === '🥔'; // Only collect potato emoji reactions
    };

    // Create the collector
    const collector = gameMessage.createReactionCollector({ filter, time: 60000 });

    // Create a set to store user IDs (to avoid duplicate entries)
    const userIds = new Set();
    collector.on('collect', (reaction, user) => {
      if (!user.bot) { // Ignore bot reactions
        userIds.add(user.id);
        interaction.channel.send(`${user} joined the game!`);
      }
    });


    collector.on('end', collected => {
      if (userIds.size > 1) {
        interaction.channel.send(`Game is starting with ${userIds.size} players.`);
        // Start the game
        startGame(Array.from(userIds), interaction.channel, punishment);
      } else {
        interaction.channel.send('Not enough players joined the game. Game cancelled.');
      }
    });
  }
}
async function startGame(players, channel, punishment) {
  // Choose a random player to start the game
  let currentHolderIndex = Math.floor(Math.random() * players.length);
  let currentHolderId = players[currentHolderIndex];

  // Inform the channel who has the potato
  await channel.send(`<@${currentHolderId}> has the potato! Pass it by saying "pass" followed by the player's mention!`);

  // Generate a random time between 15 and 50 seconds for the collector to run for
  const collectorTime = Math.floor(Math.random() * (50000 - 15000 + 1)) + 15000;

  // Filter accepts 'pass' from the current holder and 'take' from any player
  const filter = (m) => {
    return (m.content.toLowerCase().startsWith('pass') && m.author.id === currentHolderId)
      || (m.content.toLowerCase().startsWith('take') && m.author.id !== currentHolderId && players.includes(m.author.id));
  };

  const collector = channel.createMessageCollector({ filter, time: collectorTime });

  collector.on('collect', async (m) => {
    // Parse the mentioned user from the message
    const mentionedUser = m.mentions.users.first();

    if (m.content.toLowerCase() === 'pass') {
      // If only "pass" is given, choose a random player excluding the current holder
      let newHolderIndex;
      do {
        newHolderIndex = Math.floor(Math.random() * players.length);
      } while (newHolderIndex === currentHolderIndex);
      currentHolderIndex = newHolderIndex;
      currentHolderId = players[currentHolderIndex];
      // Inform the channel who has the potato
      await channel.send(`<@${currentHolderId}> has the potato!`);
    } else if (m.content.toLowerCase() === 'take' && players.includes(m.author.id) && m.author.id !== currentHolderId) {
      // If "take" is written by a valid player who is not the current holder, they steal the potato
      currentHolderId = m.author.id;
      currentHolderIndex = players.indexOf(m.author.id);
      // Inform the channel who has the potato
      await channel.send(`<@${currentHolderId}> has stolen the potato!`);
    } else if (mentionedUser && players.includes(mentionedUser.id)) {
      // If a user is mentioned and they are a valid player, pass the potato to them
      currentHolderId = mentionedUser.id;
      currentHolderIndex = players.indexOf(mentionedUser.id);
      // Inform the channel who has the potato
      await channel.send(`<@${currentHolderId}> has the potato!`);
    } else {
      // If the pass was invalid or the steal failed, inform the current holder
      await channel.send(`Invalid pass or steal, <@${currentHolderId}> still has the potato!`);
    }
  });

  collector.on('end', collected => {
    channel.send(`Game over! <@${currentHolderId}> lost the game!`);
    punish(channel, currentHolderId, punishment)
  });
}


async function punish(channel, player, punishment) {
  if (punishment.hasOwnProperty('shame')) {
    await channel.send(`Shame on <@${player}>!`);
    await addShameByUserId(channel.guild, player);
    await shameUserRecord(player, 0, 86400000, 'hotpotato');
  } else if (punishment.hasOwnProperty('lines')) {
    //await channel.send(`<@${player}> must write ${punishment.lines} lines!`);
    const user = await channel.client.users.fetch(player);

    const reply = await lineUtils.assignLinesToUser(
      player,
      user.username,
      player,
      channel.guild,
      punishment.lines,
      null,
      channel.id
    );
    await channel.send(reply);
  } else if (punishment.hasOwnProperty('manual')) {
    await channel.send(`The punishment for <@${player}> is: ${punishment.manual}`);
  }
}
