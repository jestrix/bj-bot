import { SlashCommandBuilder } from 'discord.js';
import { wfmUtils } from '../../utils/wfm.mjs';

export const data = new SlashCommandBuilder()
  .setName('get_wfm_link')
  .setDescription('See a user\'s profile link')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you want the link of')
    .setRequired(true));

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  await wfmUtils.createUserProfileEmbed("", interaction, user.id);
}
