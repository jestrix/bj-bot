import { SlashCommandBuilder } from 'discord.js';
import {leaveLottery} from '../../utils/lottery.mjs';

export const data = new SlashCommandBuilder()
  .setName('leave_lottery')
  .setDescription('Leave the lottery.')

export async function execute(interaction) {
  await leaveLottery(interaction, null)
}
