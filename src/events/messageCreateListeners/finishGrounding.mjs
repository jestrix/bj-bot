import { initBrain, brain } from '../../utils/brain.mjs';
import { groundingUtils } from '../../utils/grounding.mjs';


/**
 * This tracks the user's completion of their grounding.
 * It listens for the grounding bot messages, and checks if the user has
 * completed their minimum required completion (percentage),
 * and minimum required minutes.
 *
 * If the user has not completed their grounding,
 * it displays the amount of minutes they have completed
 * and the amount they still have to go.
 *
 * If the user has completed their grounding,
 * it removes the grounding role and sends a message
 * to the user saying their grounding is over.
 */
export const finishGrounding = {
  name: 'finishGrounding',
  async execute(message) {
    if (message.author.id == '1029920668428554312') {
      // Initialize brain
      await initBrain();

      // if author is the grounding bot, only listen to the grounding bot!
      let mentions = message.mentions;
      if (mentions && mentions.users && mentions.users.size > 0) {
        // grounding bot has to mention someone
        let user = mentions.users.first(); // User object
        if (brain.data.grounding[user?.id]) {
          let grounding = brain.data.grounding[user.id];
          // user has grounding settings
          let content = message.content;
          let minutes_completed = content.match(/has completed a \d+ minute grounding/gmi);
          minutes_completed = +minutes_completed[0].match(/\d+/)[0];
          let completion = content.match(/Completion: \d+\.?\d*%/gmi);
          completion = completion[0].match(/\d+\.?\d*/)[0];
          let fail = false;
          let reply = '';
          if (grounding.minimum_percentage > 0 && +grounding.minimum_percentage > +completion) {
            reply += 'You didn\'t complete your minimum '
              + `required ${grounding.minimum_percentage}% completion. `;
            fail = true;
          }
          if (grounding.minutes_min > 0 && +grounding.minutes_min > minutes_completed) {
            reply += 'You didn\'t complete the required minimum of '
             + `${grounding.minutes_min} minutes. `;
            fail = true;
          }
          if (fail) {
            reply = 'Oh no!! ' + reply + ' This grounding doesn\'t count =(';
            message.reply(reply);
          } else {
            // You didn't fail! Good for you.
            brain.data.grounding[user.id].minutes_completed =
              +brain.data.grounding[user.id].minutes_completed + minutes_completed;
            if (brain.data.grounding[user.id].minutes_completed >= brain.data.grounding[user.id].minutes_todo) {
              // you're free!
              groundingUtils.clearGrounding(user.id, message.guild);
              message.reply('Your grounding is finally over.');
            } else {
              // not done yet
              let minutesCompleted = brain.data.grounding[user.id].minutes_completed;
              let minutesTodo = brain.data.grounding[user.id].minutes_todo;
              let reply = `You have completed ${minutesCompleted} ${minutesCompleted === 1 ? 'minute' : 'minutes'}. `;
              let togo = minutesTodo - minutesCompleted;
              reply += `**Only ${togo} ${togo === 1 ? 'minute' : 'minutes'} of grounding left to go!**`;
              message.reply(reply);
            }
            brain.write();
          }
        }
      }
    }
  }
}
