import { brain, brainKeyExistsPromise } from '../../utils/brain.mjs';
import { lineUtils } from '../../utils/lines.mjs';
import { timeUtils } from '../../utils/time.mjs';
import {addShameByUserId, shameUserRecord} from '../../utils/shame.mjs';
import Diff from 'text-diff';

export const handleLines = {
  name: 'handleLines',
  async execute(message) {
    // make sure the 'lines' key exists
    await brainKeyExistsPromise('lines');

    // Check if the author of the message has any "lines" data stored in the brain
    if (brain.data.lines[message.author.id]) {
      let author = message.author.id;
      let data = brain.data.lines[author];

      // Check if the message was sent in the same channel as the lines were started in
      if (message.channelId == data.channel) {
        // Check if the message content is equal to the line the user was supposed to type
        if (message.content == data.line) {
          if (!lineUtils.checkForCheating(message.content, lineUtils.calculateCPM(message.content, data.last_time))) {
            // Line typed is correct
            message.react('✅');

            // Increment the number of lines completed by the user and save to the brain
            data.lines_completed += 1;
            brain.write();
          }else {
             // Shame the user when they cheat!
             message.react('❌');
             data.hasCheated = true;
             message.reply('CHEATER! Shame on you!');
             const chan = message.client.channels.cache.get(data.cmd_channel);
             chan.send(data.set_by_id != message.author.id ? `SHAME on <@${message.author.id}> for trying to cheat while doing lines for <@${data.set_by_id}>` :`SHAME on <@${message.author.id}> for trying to cheat on their lines!!`);
             await addShameByUserId(message.guild, message.author.id);
             // shame for 24 hours
             await shameUserRecord(message.author, 0, 86400000, 'cheating_on_lines');
          }
          if(!data.started) {
            data.started = true;
            data.start_time = Date.now();
          }else{
            data.current_cpm += lineUtils.calculateCPM(message.content, data.last_time);
          }
          data.last_time = Date.now();

          // Check to see if user is finished with the lines
          if (data.lines_completed >= data.lines_todo) {
            // Calculate how long it took for the lines to be completed
            let msTaken = Date.now() - data.start_time;
            if (data.goal_time > 0) {
              // Goal time has been set, check if the user has completed their lines in time
              if (timeUtils.msToSeconds(msTaken) < data.goal_time) {
                sendSuccessMessage(message, data, msTaken);
              } else {
                // The user did not meet the goal time set for them, announce that they have failed.
                sendFailureMessage(message, data, msTaken, data.goal_time);
              }
            } else {
              // No goal time set, the user has successfully completed their lines
              sendSuccessMessage(message, data, msTaken);
            }

            // Delete the channel in 10 seconds
            setTimeout(function() {
              if (message.guild.channels.cache.get(message.channelId) != undefined) {
                message.channel.delete();
              }
            }, 10000);
          } else if (data.random == true) {
            // User hasn't completed their lines, give them a new one
            const newLine = lineUtils.generateRandomLine(10, 13, 0.5);
            data.line = newLine;
            data.mistake_on_line = 0;
            brain.write();
            let displayLine = lineUtils.anticheatFilter(newLine);
            if (!data.hide_number_completed) {
              message.reply(`**Line ${(data.lines_completed + 1)} / ${data.lines_todo}**\nYour new line is: \n **\u{200B}${displayLine}\u{200B}**`);
            } else {
              message.reply(`Your new line is: \n **\u{200B}${displayLine}\u{200B}**`);
            }
          }
        } else if (message.content.toLowerCase() == 'cancel') {
          // Cancel command given, cancel this session
          message.reply('Lines cancelled. This channel will be deleted in 10 seconds!');
          lineUtils.clearLines(author, message.guild);
          const chan = message.client.channels.cache.get(data.cmd_channel);
          chan.send(`<@${author}> cancelled their lines.`);
          // Delete the channel in 10 seconds
          setTimeout(function() {
            if (message.guild.channels.cache.get(message.channelId) != undefined) {
              message.channel.delete();
            }
          }, 10000);
        } else {
          // Line was incorrect
          message.react('❌');
          data.mistakes += 1;
          data.mistake_on_line += 1;
          brain.write();
          if (lineUtils.checkForCheating(message.content, lineUtils.calculateCPM(message.content, data.last_time))) {
            // Shame the user when they cheat!
            data.hasCheated = true;
            message.reply('CHEATER! Shame on you!');
            const chan = message.client.channels.cache.get(data.cmd_channel);
            chan.send(data.set_by_id != message.author.id ? `SHAME on <@${message.author.id}> for trying to cheat while doing lines for <@${data.set_by_id}>` :`SHAME on <@${message.author.id}> for trying to cheat on their lines!!`);
            await addShameByUserId(message.guild, message.author.id);
            // shame for 24 hours
            await shameUserRecord(message.author, 0, 86400000, 'cheating_on_lines');
          } else if (data.mistake_on_line > 2) {
            let hint = generateHint(data.line, message.content);
            message.channel.send(hint);
          }
          if(!data.started) {
            data.started = true;
            data.start_time = Date.now();
          }else{
            data.current_cpm += lineUtils.calculateCPM(message.content, data.last_time);
          }
          data.last_time = Date.now();
        }
      }
    }
  }
}

function generateHint(expectedLine, givenLine) {
  let hint = 'Hint: ';
  // Find differences in the lines
  let diff = new Diff();
  let textDiff = diff.main(givenLine, expectedLine);
  for (let i = 0; i < textDiff.length; i++) {
    let op = textDiff[i][0]; // Operation: -1 = delete, 1 = add, 0 = no change
    let data = textDiff[i][1];
    switch (op) {
      // Nothing needs to change in this section
      case 0: {
        hint += data;
        break;
      }
      // Extra character / space
      case -1: {
        hint += '~~**' + data + '**~~';
        break;
      }
      // Character or space missing.
      case 1: {
        const newdata = replaceAll(data, ' ', '_');
        hint += '**' + newdata + '**';
        break;
      }
      default:
        break;
    }
  }
  return lineUtils.anticheatFilter(hint);
}


function sendSuccessMessage(message, data, timeTaken) {
  message.reply(`Congratulations! You have successfully completed your lines after ${timeUtils.msToTime(timeTaken)}! This channel will be deleted in 10 seconds.`);
  let numMistakes = data.mistakes;
  lineUtils.clearLines(message.author.id, message.guild);
  const chan = message.client.channels.cache.get(data.cmd_channel);
  let reply_msg = ``;
  if(data.set_by_id != message.author.id) {
    reply_msg += `<@${message.author.id}> completed their lines for <@${data.set_by_id}>. `
  }else{
    reply_msg += `<@${message.author.id}> completed their lines.`;
  }
  reply_msg += ` They made **${numMistakes}** ${numMistakes == 1 ? "mistake" : "mistakes"} and it took them **${timeUtils.msToTime(timeTaken)}** ${data.lines_todo > 1 ? "with a typing speed of **" + lineUtils.calculateFinalWPM(data.current_cpm, data.lines_todo - 1) + " WPM!**" : "."}`;
  if(data.hasCheated) {
    reply_msg += `\n\n**However, they also cheated during this session!**`
  }
  chan.send(reply_msg);
}

function sendFailureMessage(message, data, timeTaken, goal_time) {
  message.reply(`You have failed yours lines as you did not complete them in the goal time. You took ${timeUtils.msToTime(timeTaken)}`)
  const chan = message.client.channels.cache.get(data.cmd_channel);
  chan.send(`<@${message.author.id}> failed to complete their lines in the goal time of ${timeUtils.secsToTime(goal_time)}`);
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
