import {lineUtils} from '../utils/lines.mjs';
import {forceLeaveLottery} from '../utils/lottery.mjs';
import {brain} from '../utils/brain.mjs';

const guildMemberRemove = {
    name: 'guildMemberRemove',
    async execute(member) {
        
        // Remove player from the lottery if they had joined
        await forceLeaveLottery(member)

        try {
            // Handle if the user had a lines channel.
            if(brain.data.lines[member.user.id]) {
                let savedChannelId = brain.data.lines[member.user.id].channel;
                let msgChannel = member.guild.channels.cache.get(brain.data.lines[member.user.id].cmd_channel);
                msgChannel.send(`${brain.data.lines[member.user.id].username} left the server before they could finish their lines :(`);
                lineUtils.clearLines(member.user.id, member.guild);
                if(member.guild.channels.cache.get(savedChannelId) != undefined) {
                    member.guild.channels.cache.get(savedChannelId).delete();
                }
            }
        } catch(error){
            console.log(error)
        }
    }
};

export default guildMemberRemove;